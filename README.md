# apiary interview test

#### Altough I had no experience with, I built this using vuejs because:
- It's a light but yet powerfull, fast and comes with the nicest syntax of all the current frameworks
- Has a great boilerplate (that I used here)
- You showed interest in adopting it
- It's just nice :)

#### Because of my limited available time this was left out:
- Automated tests (shame!)
- Typescript and JSDoc
- Apiary instead of the hardcoded mock data
- Schema.org vocabulary (yet this isn't important) and  aria attributes for accessibility
- Responsive CSS
- Fancier animations

#### This features were added:
- Search is performed also on the subdomain and owner
- Button to add project to favourites - sticking it on the top of the list
- Extra info: type, number of users, GitHub connected, public/private
- Keyboard navigation inside projectes using arrow keys, escape to clear search/close the switcher
- Projects sorted by favourite flag, then by lastOpened date

For more info please check the comments inside the source files


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).