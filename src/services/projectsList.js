/* mock API to be replaced with Apiary API */
let data = [{
  title: 'project a', // property must be HTML stripped on save, to prevent xss attacks
  owner: 'Yours', // property must be HTML stripped on save, to prevent xss attacks
  subdomain: 'gofitio', // property must be HTML stripped on save, to prevent xss attacks
  isPublic: false,
  format: 'blueprint',
  isGithubConnected: true,
  isFavourite: true,
  numberUsers: 2,
  lastOpened: 1498406040 // unix timestamp used to sort projects
}, {
  title: 'project b',
  owner: 'someone',
  subdomain: 'nicedomain',
  isPublic: true,
  format: 'swagger',
  isGithubConnected: true,
  isFavourite: false,
  numberUsers: 100,
  lastOpened: 1499406040
}, {
  title: 'project c',
  owner: 'someoneelse',
  subdomain: 'nicedomain2',
  isPublic: true,
  format: 'swagger',
  isGithubConnected: false,
  isFavourite: true,
  numberUsers: 121,
  lastOpened: 1497406040
}, {
  title: 'project d',
  owner: 'someoneelse',
  subdomain: 'project',
  isPublic: true,
  format: 'swagger',
  isGithubConnected: false,
  isFavourite: false,
  numberUsers: 121,
  lastOpened: 1497406039
}, {
  title: 'project e',
  owner: 'someoneelse',
  subdomain: 'onedomain',
  isPublic: true,
  format: 'swagger',
  isGithubConnected: false,
  isFavourite: false,
  numberUsers: 121,
  lastOpened: 1497406040
}]

// fields where the search should perform
const SEARCH_FIELDS = ['title', 'owner', 'subdomain']

export default {
  methods: {
    _sortByFavouriteFlag (a, b) {
      // isFavourite == true first
      return (a.isFavourite === b.isFavourite) ? 0 : a.isFavourite ? -1 : 1
    },
    _sortByLastOpened (a, b) {
      return new Date(a.lastOpened) - new Date(b.lastOpened)
    },
    _getStartIndex (string, query) {
      return string.toLowerCase().indexOf(query.toLowerCase())
    },
    _getEndIndex (string, query) {
      if (this._getStartIndex(string, query) > -1) {
        return this._getStartIndex(string, query) + query.length
      }
      return -1
    },
    // TODO: improve the filter to give higher rank according to fields order in the array
    _searchFilter (query) {
      return (project) => {
        let matches = false

        SEARCH_FIELDS.forEach(field => {
          if (this._getStartIndex(project[field], query) > -1) {
            matches = true
          }
        })
        return matches
      }
    },
    _underlineSearchText (query) {
      const underline = (string, query) => {
        let textBefore = string.substring(0, this._getStartIndex(string, query))
        let matchedText = string.substring(
          this._getStartIndex(string, query),
          this._getEndIndex(string, query)
        )
        let textAfter = string.substring(
          this._getEndIndex(string, query),
          string.length
        )
        return `${textBefore}<u>${matchedText}</u>${textAfter}`
      }

      return (project) => {
        // clone object otherwise it will messup with the original data
        let projectClone = JSON.parse(JSON.stringify(project))

        SEARCH_FIELDS.forEach(field => {
          projectClone[field] = underline(projectClone[field], query)
        })
        return projectClone
      }
    },
    getProjects () {
      // TODO: replace with a request to a server
      return Promise.resolve(
        data.sort(this._sortByLastOpened)
          .sort(this._sortByFavouriteFlag)
      )
    },
    searchProjects (query) {
      return Promise.resolve(
        data.filter(this._searchFilter(query))
          .map(this._underlineSearchText(query))
      )
    }
  }
}
